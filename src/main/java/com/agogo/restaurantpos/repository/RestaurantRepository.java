package com.agogo.restaurantpos.repository;

import com.agogo.restaurantpos.model.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by derrickgoh on 11/9/18.
 */
public interface RestaurantRepository extends JpaRepository<Restaurant, Long>{


}
